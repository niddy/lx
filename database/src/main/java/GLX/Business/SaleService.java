package GLX.Business;

import GLX.DAO.BookDAO;
import GLX.Model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class SaleService {
    BookDAO bookDAO;
    @Autowired
    public void setBookDAO(BookDAO bookDAO){
        this.bookDAO=bookDAO;
    }
    public void insert(Book book){
        bookDAO.insertBook(book);
    }
    @Transactional
    public void onSale(float sale){
        List<Book> bookList= bookDAO.queryAll();
        for(Book b: bookList){
            b.setPrice(b.getPrice()*sale);
            bookDAO.modifyBook(b);
        }
    }

}
