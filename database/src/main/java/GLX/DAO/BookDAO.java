package GLX.DAO;

import GLX.Model.Book;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface BookDAO {
    void insertBook(Book book);
    void insertAll(List<Book>bookList);
    void deleteBook(Book book);
    void deleteAll(List<Book>bookList);
    void deleteById(String id);
    void modifyBook(Book book);
    Book queryOneBook(Book book);
    Book queryOneBookById(String id);
    List<Book> queryAll();
}
