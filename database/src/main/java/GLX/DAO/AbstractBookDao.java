package GLX.DAO;

import GLX.Model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import java.util.List;

public abstract class AbstractBookDao implements BookDAO{
    List<Book> books;
    JdbcTemplate jdbcTemplate;
    public void setBooks(List<Book> books){
        this.books=books;
    }
    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate=jdbcTemplate;
    }

    @Override
    public void insertBook(Book book) {

    }

    @Override
    public void insertAll(List<Book> bookList) {

    }

    @Override
    public void deleteBook(Book book) {

    }

    @Override
    public void deleteAll(List<Book> bookList) {

    }

    @Override
    public void deleteById(String id) {

    }

    @Override
    public void modifyBook(Book book) {

    }

    @Override
    public Book queryOneBook(Book book) {
        return null;
    }

    @Override
    public Book queryOneBookById(String id) {
        return null;
    }

    @Override
    public List<Book> queryAll() {
        return null;
    }
}
