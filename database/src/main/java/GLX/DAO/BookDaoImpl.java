package GLX.DAO;

import GLX.Model.Book;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;


import java.util.List;
@Repository(value = "bookDaoImpl")
public class BookDaoImpl extends AbstractBookDao {

    @Override
    public void insertBook(Book book) {
        super.insertBook(book);
        String INSERT_BOOK="INSERT INTO book(isdn,name,price) VALUES(?,?,?)";
        Object[] args={book.getIsdn(),book.getName(),book.getPrice()};
        jdbcTemplate.update(INSERT_BOOK, args);

    }

    @Override
    public void modifyBook(Book book) {
        super.modifyBook(book);
        String MODIFY_BOOK ="update book set isdn=?,name=?,price=?";
        Object[] args={book.getIsdn(),book.getName(),book.getPrice()};
        jdbcTemplate.update(MODIFY_BOOK,args);
    }


    @Override
    public List<Book> queryAll() {
        String QUERY_all_Book="select * from book";
        RowMapper<Book> rowMapper=new BeanPropertyRowMapper<Book>(Book.class);
        books = jdbcTemplate.query(QUERY_all_Book, rowMapper);
        return books;
    }


}
