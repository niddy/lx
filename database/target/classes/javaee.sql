create table book
(
    name  varchar(255)   null,
    isdn  varchar(255)   null,
    price decimal(10, 2) null
);

